import { Component } from '@angular/core';
import { item } from '../utils/classes';
import { ActivatedRoute } from "@angular/router";
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
	selector: 'app-tab3',
	templateUrl: 'tab3.page.html',
	styleUrls: ['tab3.page.scss']
})

export class Tab3Page {

	item: item;
	listaEquipamentos: Array<item>;


	constructor(private route: ActivatedRoute, private storage: Storage, private router: Router) {
		this.route.queryParams.subscribe(params => {
			this.item = JSON.parse(params.item)
			// this.item.detalhes = this.item.detalhes.replace(/'↵'/g, '<br>'); // não consegui arrumar a quebra de linha
			// console.log(this.item);
		});
		this.storage.get('lista').then((val) => {
			this.listaEquipamentos = val;
		});
	}

	receberItem() {
		this.listaEquipamentos.forEach(element => {
			if (this.item.id === element.id) {
				let temp = element;
				temp.emprestado = false;
				temp.detalhes = '';
				element = temp;
				this.storage.set('lista', this.listaEquipamentos);
				this.router.navigate(["tabs/tab1"]);
			}
		});
	}
}
