import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { item } from '../utils/classes';

import { Storage } from '@ionic/storage';

@Component({
	selector: 'app-tab1',
	templateUrl: 'tab1.page.html',
	styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

	listaEquipamentos: Array<item>;
	formulario: any;

	constructor(private fb: FormBuilder, private storage: Storage, private router: Router) {
		this.formulario = this.fb.group({
			nome: [null, Validators.required],
			detalhes: ''
		});
	}

	ionViewWillEnter() {
		this.storage.get('lista').then((val) => {
			this.listaEquipamentos = val;
		});
	}

	deletarItem(item: item) {
		this.listaEquipamentos.forEach(element => {
			if (element.id === item.id) {
				this.listaEquipamentos.splice(this.listaEquipamentos.indexOf(item), 1);
			}
		});
		this.storage.set('lista', this.listaEquipamentos);
	}
	
	mostrarDetalhes(item: item) {
		let navigationExtras = {
			queryParams: {
				item: JSON.stringify(item)
			}
		};
		
		this.router.navigate(["tabs/tab3"], navigationExtras);
	}

	adicionarItem() {
		if (this.listaEquipamentos === null || this.listaEquipamentos.length === 0) {
			this.listaEquipamentos = [];
			this.formulario.value.id = 100;
			this.formulario.value.emprestado = false;
			this.listaEquipamentos.push(this.formulario.value);
		} else {
			let temp = JSON.parse(JSON.stringify(this.formulario.value));
			temp.id = JSON.parse(JSON.stringify((this.listaEquipamentos[this.listaEquipamentos.length - 1]).id));
			temp.id++
			temp.emprestado = false;
			this.listaEquipamentos.push(temp);
		}
		console.log(this.listaEquipamentos);
		this.storage.set('lista', this.listaEquipamentos);
		this.formulario.reset();
	}
}
