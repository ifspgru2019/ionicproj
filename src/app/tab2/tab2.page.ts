import { Component } from '@angular/core';
import { item } from '../utils/classes';
import { Storage } from '@ionic/storage';

@Component({
	selector: 'app-tab2',
	templateUrl: 'tab2.page.html',
	styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

	listaEquipamentos: Array<item>;
	listaFiltrada: Array<item>;
	itemSelecionado: number;
	erro: boolean;
	sucesso: boolean;
	emprestimo: string;
	placeholder: string;

	constructor(private storage: Storage) {
	}

	ionViewWillEnter() {
		this.erro = null;
		this.sucesso = null;
		this.storage.get('lista').then((val) => {
			this.listaEquipamentos = val;
			this.listaFiltrada = [];
			this.listaEquipamentos.forEach(element => {
				if (element.emprestado === false) {
					this.listaFiltrada.push(element);
				}
			});
		});
	}

	alteraItem() {
		if (this.itemSelecionado === undefined) {
			this.erro = true;			
		} else {
			this.listaEquipamentos.forEach(item => {
				if (item.id === Number(this.itemSelecionado)) {
					let temp = item;
					temp.detalhes = this.emprestimo;
					temp.emprestado = true;
					this.listaEquipamentos[this.listaEquipamentos.indexOf(item)] = temp;
					this.storage.set('lista', this.listaEquipamentos);
					this.erro = false;
					this.sucesso = true;
					this.emprestimo = '';
					this.placeholder = '';
				}
			});
		}
	}

	novaSelecao(novo){
		this.itemSelecionado = novo.detail.value;
	}
}
